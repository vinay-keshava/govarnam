From: =?utf-8?q?Guido_G=C3=BCnther?= <agx@sigxcpu.org>
Date: Wed, 20 Sep 2023 17:42:53 +0200
Subject: Limit exported symbols (#35)

* build: Set soname on Linux

The merge of MR dropped `else` which made the soname setting
ineffective on Linux.

* Limit exported symbols

This greatly reduces the number of exported symbols by limiting the
namespace of exported symbols to {varnam,varray,vm}_ thus avoiding the
export of all the `_cg` symbols from go.

Before:

$ readelf -s /tmp/a/usr/lib/libgovarnam.so.1.9.0  | grep -v " UND " | wc -l
248

After:

$ readelf -s /usr/lib/libgovarnam.so.1.9.0  | grep -v " UND " | wc -l
52

With this we can make sure applications don't link against accidentally
exported symbols and break on library upgrades without us being able to
notice. It also allows us to notice when symbols go missing and we hence
need to bump the ABI version.

(cherry picked from commit 436d0066819f1b7f5a50f3d458b1ca056ac74f1a)
---
 Makefile      | 4 +++-
 govarnam.syms | 9 +++++++++
 2 files changed, 12 insertions(+), 1 deletion(-)
 create mode 100644 govarnam.syms

diff --git a/Makefile b/Makefile
index 6a577bc..473cedb 100644
--- a/Makefile
+++ b/Makefile
@@ -17,11 +17,13 @@ UNAME := $(shell uname)
 SED := sed -i
 LIB_NAME := libgovarnam.so
 SO_NAME := $(shell (echo $(VERSION) | cut -d. -f1))
+CURDIR := $(shell pwd)
 
 ifeq ($(UNAME), Darwin)
   SED := sed -i ""
   LIB_NAME = libgovarnam.dylib
-  EXT_LDFLAGS = -extldflags -Wl,-soname,$(LIB_NAME).$(SO_NAME)
+else
+  EXT_LDFLAGS = -extldflags "-Wl,-soname,$(LIB_NAME).$(SO_NAME),--version-script,$(CURDIR)/govarnam.syms"
 endif
 
 VERSION_STAMP_LDFLAGS := -X 'github.com/varnamproject/govarnam/govarnam.BuildString=${BUILDSTR}' -X 'github.com/varnamproject/govarnam/govarnam.VersionString=${VERSION}' $(EXT_LDFLAGS)
diff --git a/govarnam.syms b/govarnam.syms
new file mode 100644
index 0000000..8e5bdd1
--- /dev/null
+++ b/govarnam.syms
@@ -0,0 +1,9 @@
+{
+        global:
+                varnam_*;
+                varray_*;
+                vm_*;
+        local:
+                *;
+};
+
